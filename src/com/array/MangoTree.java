package com.array;

import java.util.Scanner;

public class MangoTree {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter row, column and tree number to find ");
        int row = scanner.nextInt();
        int column = scanner.nextInt();
        int treeNumber = scanner.nextInt();
        boolean mangoTree = false;

        if ((1 <= treeNumber && treeNumber <= column) || (treeNumber - 1) % column == 0 || (treeNumber % column == 0)) {
            mangoTree = true;
        } else {
            mangoTree = false;
        }
        System.out.println("Is mango tree is possible? " + (mangoTree ? "Yes" : "No"));
    }
}
